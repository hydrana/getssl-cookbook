# getssl

Installs getssl, from https://github.com/srvrco/getssl, and configures it.

Finally a cron job will be created in order to execute the `getssl -u -a -q`
command daily at 5:23 AM.

This cookbook doesn't support ACL per domain's SANS, therefor is sets the
USE_SINGLE_ACL to `true`. That means the `ACL` will be used to validate the
domain and its sub-domains, which means the same server(s) is/are used to
validate both the root domain and its sub-domain.

## Requirements

### Platforms

* Ubuntu 20.04 LTS

### Chef

* Chef >= 16

## Attributes

So far this cookbook handles only one domain and only few options from
the getssl config files, just what we needed, but adding more is easy.

Have a look at the [attributes file](/attributes/default.rb) in order to see
what can be configured.

## Usage

Add the `getssl` cookbook to your `run_list`.

Here is a very basic setup for the `example.com` domain:

```ruby
default['getssl'] = {
  account: {
    email: 'youremail@domain.tld'
  },
  domain: {
    acl: [
      '/var/www/example.com/web/.well-known/acme-challenge'
    ],
    name: 'example.com',
    reload_cmd: 'systemctl restart nginx'
  }
}
```

### With many web nodes

Given you have many nodes serving the traffic for the same domain name, getssl
will address the 2 issues that use case has:

1. Making the challenge file available to all your nodes using the
   **A**cme **C**hallenge **L**ocation (ACL)
2. Copying the generated certificate files to all your nodes and reload/restart
   the web server

#### Acme Challenge Location (ACL)

You *must* define at least one, which is the local one to the current node being
converged by Chef.\
Then you can define additionnal ACLs to push the challenge file to your other
nodes through SSH/SFTP/FTPS:

```ruby
default['getssl']['domain']['acl'] = [
  # First one is the local file location for the current node
  '/var/www/your_app/current/.well-known/acme-challenge',

  # Then your other nodes through SSH for example
  'ssh:admin@192.168.1.23:/var/www/your_app/current/.well-known/acme-challenge',
  'ssh:admin@192.168.1.24:/var/www/your_app/current/.well-known/acme-challenge'
]
```

See the getssl documentation or the [attributes file](/attributes/default.rb)
for more examples.

#### Copying the generated certificate files to all your nodes

After the challenge succeeded, you will need to get the certificate files being
copied to all your other nodes and the webserver being restarted/reloaded.

Here is how to configure the certificate files copying:

```ruby
default['getssl']['domain']['key_location'] = [
  # First line for the current node
  '/etc/nginx/pki/private/server.key',

  # Then your other nodes through SSH for example
  'ssh:admin@192.168.1.23:/etc/nginx/pki/private/server.key',
  'ssh:admin@192.168.1.24:/etc/nginx/pki/private/server.key'
]

default['getssl']['domain']['chain_location'] = [
  # First line for the current node
  '/etc/nginx/pki/domain-chain.crt',

  # Then your other nodes through SSH for example
  'ssh:admin@192.168.1.23:/etc/nginx/pki/domain-chain.crt',
  'ssh:admin@192.168.1.24:/etc/nginx/pki/domain-chain.crt'
]
```

Here is how to configure the local and remote reload of the web server:

```ruby
default['getssl']['domain']['reload_cmd'] = [
  # First line for the current node
  'systemctl reload nginx',

  # Then your other nodes through SSH for example
  'ssh:admin@192.168.1.23:systemctl reload nginx',
  'ssh:admin@192.168.1.24:systemctl reload nginx'
]
```

#### Hey! Automation now!

Okay so quite boring to repeat those IP addresses and/or adding new nodes
manually ?
Are you looking for an automated way of doing this ?

After all, it's Chef !

Given all your nodes are identical (same apps, same pathes for files), then just
define the first line only, and set
the `default['getssl']['domain']['auto'] = true` attribute and this cookbook
will build the lines for all your nodes (searched from your chef repository)
having this cookbook in their `run_list`:

```ruby
default['getssl'] = {
  account: {
    email: 'youremail@domain.tld'
  },
  domain: {
    acl: [
      '/var/www/example.com/web/.well-known/acme-challenge'
    ],
    auto: true,
    chain_location: '/etc/nginx/pki/domain-chain.crt',
    key_location: '/etc/nginx/pki/private/server.key',
    name: 'example.com',
    reload_cmd: 'systemctl reload nginx'
  }
}
```

And Voila, your other nodes will be included automatically in the `acl` and
`reload_cmd` attribute at runtime, so don't forget to converge all your nodes so
that each nodes know about the others and have a complete list to copy/restart.

## Recipes

```ruby
recipe[rvm_io::default] # Install, configure getssl and create a cron job
```

## Author

Hydrana SAS
