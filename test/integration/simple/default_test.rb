# frozen_string_literal: true

# Chef InSpec test for recipe getssl-cookbook::default

# The Chef InSpec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec/resources/

describe package('bind9-host') do
  it { should be_installed }
end

describe file('/usr/local/bin/getssl') do
  it { should exist }

  its('mode') { should cmp '0700' }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
end

describe file('/root/.getssl/getssl.cfg') do
  it { should exist }

  its('content') { should match(%r{^CA="https://acme-staging-v02.api.letsencrypt.org/"$}) }
  its('content') { should match(/^ACCOUNT_EMAIL="john@doe.movie"$/) }

  its('mode') { should cmp '0644' }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
end

describe file('/root/.getssl/doe.movie/getssl.cfg') do
  it { should exist }

  its('content') { should match(/^#SCP_OPTS=""$/) }
  its('content') { should match(/^#SSH_OPTS=""$/) }
  its('content') do
    should match(%r{^ACL=\('/var/www/doe\.movie/web/\.well-known/acme-challenge'\)$})
  end
  its('content') { should match(/^#DOMAIN_CHAIN_LOCATION=""$/) }
  its('content') { should match(/^#DOMAIN_KEY_LOCATION=""$/) }
  its('content') { should match(/^RELOAD_CMD="service nginx reload"$/) }

  its('mode') { should cmp '0644' }
  its('owner') { should eq 'root' }
  its('group') { should eq 'root' }
end

describe crontab('root').commands('getssl -u -a -q') do
  its('minutes') { should cmp '23' }
  its('hours') { should cmp '5' }
end
