# frozen_string_literal: true

### Global getssl configuration (/root/.getssl/getssl.cfg)

## Let's Encrypt environment base URL
#
# You can set this with the staging base URL
# * https://acme-staging-v02.api.letsencrypt.org
#
# or the production base URL (default)
# * https://acme-v02.api.letsencrypt.org
default['getssl']['ca'] = 'https://acme-v02.api.letsencrypt.org'

## Email address used to notify you about renewal delays
default['getssl']['account']['email'] = nil

## Length of the generated key by Let's Encrypt
default['getssl']['account']['key_length'] = 4096

### Domain getssl configuration (/root/.getssl/<domain>/getssl.cfg)

default['getssl']['domain']['name'] = nil

# Additional domains - this could be multiple domains / subdomains in a comma separated list
# Note: this is Additional domains - so should not include the primary domain.
#
# This attribute should be an Array of String of the expected values for this
# attribute.
default['getssl']['domain']['sans'] = []

## Specify SSH options, e.g. non standard port in SSH_OPTS
# (Can also use SCP_OPTS and SFTP_OPTS)
default['getssl']['domain']['scp_options'] = nil
default['getssl']['domain']['sftp_options'] = nil
default['getssl']['domain']['ssh_options'] = nil

## SCP, SFTP and SSH user to be used remotely
#
# When the auto mode is enabled, the user given here will be used in order to
# authenticate against the other nodes.
# Leave it empty to use the user running getssl.
default['getssl']['domain']['remote_user'] = nil

# Acme Challenge Location. The first line for the domain, the following ones for each additional domain.
# If these start with ssh: then the next variable is assumed to be the hostname and the rest the location.
# An ssh key will be needed to provide you with access to the remote server.
# Optionally, you can specify a different userid for ssh/scp to use on the remote server before the @ sign.
# If left blank, the username on the local server will be used to authenticate against the remote server.
# If these start with ftp:/ftpes:/ftps: then the next variables are ftpuserid:ftppassword:servername:ACL_location
# These should be of the form "/path/to/your/website/folder/.well-known/acme-challenge"
# where "/path/to/your/website/folder/" is the path, on your web server, to the web root for your domain.
# ftp: uses regular ftp; ftpes: ftp over explicit TLS (port 21); ftps: ftp over implicit TLS (port 990).
# ftps/ftpes support FTPS_OPTIONS, e.g. to add "--insecure" to the curl command for hosts with self-signed certificates.
# You can also user WebDAV over HTTPS as transport mechanism. To do so, start with davs: followed by username,
# password, host, port (explicitly needed even if using default port 443) and path on the server.
# Multiple locations can be defined for a file by separating the locations with a semi-colon.
# default['getssl']['domain']['acl'] = [
#     '/var/www/example.com/web/.well-known/acme-challenge',
#     'ssh:server5:/var/www/example.com/web/.well-known/acme-challenge',
#     'ssh:sshuserid@server5:/var/www/example.com/web/.well-known/acme-challenge',
#     'ftp:ftpuserid:ftppassword:example.com:/web/.well-known/acme-challenge',
#     'davs:davsuserid:davspassword:{DOMAIN}:443:/web/.well-known/acme-challenge',
#     'ftps:ftpuserid:ftppassword:example.com:/web/.well-known/acme-challenge',
#     'ftpes:ftpuserid:ftppassword:example.com:/web/.well-known/acme-challenge'
# ]
default['getssl']['domain']['acl'] = [
  # Insert here your Acme Challenge Locations
]

# Set this to true if you need the full chain file to include the root certificate
default['getssl']['domain']['full_chain_include_root'] = nil

# Location for all your certs, these can either be on the server (full path name)
# or using ssh /sftp as for the ACL
#
# You can pass a string or an Array if you'd like to also push the certificate
# key to other remote nodes.
default['getssl']['domain']['chain_location'] = nil
default['getssl']['domain']['key_location'] = nil

# The command needed to reload apache / nginx or whatever you use.
# Several (ssh) commands may be given using a bash array:
# RELOAD_CMD=('ssh:sshuserid@server5:systemctl reload httpd' 'logger getssl for server5 efficient.')
#
# You can pass a string or an Array if you'd like to also restart/reload a
# service from other remote nodes.
default['getssl']['domain']['reload_cmd'] = nil

# Automatic mode !
#
# When this is `true`, all the `acl`, `key_location`, `chain_location`, and
# `reload_cmd` will be converted to Array and filled with one line for each of
# the nodes having this cookbook in their `run_list` searched from your Chef
# repository.
default['getssl']['domain']['auto'] = false
