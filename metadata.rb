name 'getssl'
maintainer 'Hydrana SAS'
maintainer_email 'contact at hydrana dot io'
license 'MIT'
description 'Installs/Configures getssl'
version '0.6.1'
chef_version '>= 16.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://gitlab.com/hydrana/getssl-cookbook/-/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://gitlab.com/hydrana/getssl-cookbook'

supports 'ubuntu'
