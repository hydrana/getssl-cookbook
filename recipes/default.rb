# frozen_string_literal: true

#
# Cookbook:: getssl-cookbook
# Recipe:: default
#
# Copyright:: 2022, The Authors, All Rights Reserved.

package 'bind9-host' do
  action :install
end

remote_file '/usr/local/bin/getssl' do
  action :create
  source 'https://raw.githubusercontent.com/srvrco/getssl/latest/getssl'
  mode '0700'
  owner 'root'
  group 'root'

  not_if { File.exist?('/usr/local/bin/getssl') }
end

execute "getssl -c #{node['getssl']['domain']['name']}" do
  command "getssl -c #{node['getssl']['domain']['name']}"

  not_if { File.exist?("~/.getssl/#{node['getssl']['domain']['name']}") }
end

template '/root/.getssl/getssl.cfg' do
  action :create

  source 'getssl.cfg.erb'
  variables(
    account_email: node['getssl']['account']['email'],
    ca: node['getssl']['ca'],
    key_length: node['getssl']['account']['key_length']
  )

  mode '0644'
  owner 'root'
  group 'root'
end

ruby_block 'auto mode' do
  action :run
  block do
    node.run_state['acl'] = node['getssl']['domain']['acl']
    node.run_state['chain_location'] = node['getssl']['domain']['chain_location']
    node.run_state['key_location'] = node['getssl']['domain']['key_location']
    node.run_state['reload_cmd'] = node['getssl']['domain']['reload_cmd']

    remote_user = node['getssl']['domain']['remote_user']

    if node['getssl']['domain']['auto']
      peers = search(:node, 'run_list:*getssl\:\:default')
      peers.each do |peer|
        next if peer['hostname'] == node['hostname']

        remote = ''
        remote = "#{remote_user}@" if remote_user
        remote += peer['ipaddress']

        %w[acl chain_location key_location reload_cmd].each do |key|
          node.run_state[key] = Array(node.run_state[key])
          copy_string = "ssh:#{remote}:#{node.run_state[key].first}"
          # Cannot use `node.run_state[key] << copy_string` due to the following
          # error:
          #
          # Chef::Exceptions::ImmutableAttributeModification
          # ------------------------------------------------
          # Node attributes are read-only when you do not specify which precedence
          # level to set. To set an attribute use code like
          # `node.default["key"] = "value"'
          #
          # seem to be a bug in Chef ...
          node.run_state[key] = node.run_state[key] | [copy_string]
        end
      end
    end
  end
end

template "/root/.getssl/#{node['getssl']['domain']['name']}/getssl.cfg" do
  action :create

  source 'domain.getssl.cfg.erb'
  variables(
    acls: lazy { node.run_state['acl'] },
    chain_location: lazy { node.run_state['chain_location'] },
    full_chain_include_root: node['getssl']['domain']['full_chain_include_root'],
    key_location: lazy { node.run_state['key_location'] },
    reload_cmd: lazy { node.run_state['reload_cmd'] },
    sans: node['getssl']['domain']['sans'],
    scp_options: node['getssl']['domain']['scp_options'],
    sftp_options: node['getssl']['domain']['sftp_options'],
    ssh_options: node['getssl']['domain']['ssh_options']
  )

  mode '0644'
  owner 'root'
  group 'root'
end

cron 'getssl' do
  hour '5'
  minute '23'
  command 'getssl -u -a -q'
end
