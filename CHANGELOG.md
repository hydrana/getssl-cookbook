# getssl-cookbook CHANGELOG

This file is used to list changes made in each version of the getssl-cookbook cookbook.

## Unreleased

## 0.6.1

- Sets the domain's USE_SINGLE_ACL to true

## 0.6.0

- Adds the `default['getssl']['domain']['sans']` attribute

## 0.5.0

- Adds the `default['getssl']['domain']['full_chain_include_root']` attribute

## 0.4.1

- Fixes Let's Encrypt URLs

## 0.4.0

- Adds the `default['getssl']['domain']['remote_user']` attribute used in auto mode in order set the remote user to be used to copy files or running commands remotely

## 0.3.0

- Fixes ACL variable content in auto mode
- Adds SCP_OPTS and SFTP_OPTS

## 0.2.0

Prevents from re-downloading getssl (prefer the script's auto-update feature)

## 0.1.0

Initial release.
